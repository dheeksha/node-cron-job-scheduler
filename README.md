## Prerequisites

```
1. Node installed on your machine => v8.10.0
2. NPM installed on your machine => 5.6.0
```

## Installing node modules

```
cd node-cron-job-scheduler
npm install
```

## Run the server

```
npm start
```