'use strict';

const puppeteer = require('puppeteer');

const cronMyCuteBaby = async function() {
	try {
		console.log("scheduler started...");
		const browser = await puppeteer.launch({
			args: ['--no-sandbox', '--disable-setuid-sandbox'],
			ignoreDefaultArgs: ['--disable-extensions']
		});

		const page = await browser.newPage();
		await page.goto('http://mycutebaby.in/contest/participant/?n=5e5d54f6e701e&utm_source=wsapp_share&utm_campaign=March_2020&utm_medium=shared&utm_term=wsapp_shared_5e5d54f6e701e&utm_content=participant', {
			waitUntil: 'networkidle2'
		});

		await page.waitFor('input[name=v]');
		await page.$eval('input[name=v]', el => el.value = 'Ravi');

		await page.evaluate(() => {
			document.querySelector('a#vote_btn.vote-btn').click();
		});

		await page.waitFor(5000);
		const message = await page.evaluate(() => {
			return document.querySelector('p#vote_msg').textContent;
		});

		await browser.close();

		console.log(message);
		console.log("scheduler ended...");
	} catch (error) {
		console.log("Error :::", error);
	}
};

module.exports = cronMyCuteBaby;