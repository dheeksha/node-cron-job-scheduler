'use strict';

const cron = require("node-cron");
const express = require("express");

const cronMyCuteBaby = require("./mycutebaby-scrape");

const app = express();

cron.schedule("*/30 * * * *", function() {
	cronMyCuteBaby();
});

app.listen(3128);